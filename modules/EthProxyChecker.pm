#!/usr/bin/perl 
package EthProxyChecker;

use Log::Log4perl qw(:easy);
Log::Log4perl->init('log4perl.conf');

use REST::Client;
use Data::Dumper;
use JSON;

sub new {

    my $class = shift;

    my $this = {
        url => shift,
        maxUnreachableCount => shift
    };
    
    if (!defined($this->{maxUnreachableCount})) {
        WARN "Max unreachable pings parameter not defined. Assuming default 20";
        $this->{maxUnreachableCount} = 5;
    }

    $this->{unreachableCallCount} = 0;
    $this->{pinger} = Net::Ping->new("external");
    
    bless $this, $class;
    INFO "Start monitoring EthProxy $this->{url}";
    if (!$this->check()) {
        WARN "Init check: host is unreachable\n";
    } else {
        INFO "Init check: host is reachable\n"
    }

    return $this;

}

sub check() {
    my ( $this ) = @_;

    my $client = REST::Client->new();
    my $url = $this->{url};
    
    $client->setTimeout(20);
    $client->GET($url);
    
    my $response = $client->responseCode();
    if ($response == 200) {
        INFO "EthProxy - version checked - OK"
    } else {
        INFO "EthProxy: error while checking version URL: $response"
    }
    
    return response == 200;
}