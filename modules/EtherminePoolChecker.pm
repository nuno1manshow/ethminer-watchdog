#!/usr/bin/perl 
package EtherminePoolChecker;

use Log::Log4perl qw(:easy);
Log::Log4perl->init('log4perl.conf');

use REST::Client;
use Data::Dumper;
use JSON;

sub new {
    my $class = shift;
    my $this = {
        wallet => shift,
        baseUrl => shift,
        apiUrl => shift,
        alertThreshold => shift,
        maxBelowThreshold => shift,
        maxUnsuccessfulResets => shift
    };
    
    $this->{belowThresholdCount} = 0;
    $this->{totalResets} = 0;
    
    INFO "Ethermine pool checker initialized - will alert if reported hrate drops below $this->{alertThreshold} for $this->{maxBelowThreshold} consecutive readings\n";

    bless $this, $class;
    return $this;
}

sub getActualHashrate() {
    my ( $this, $string ) = @_;
    my ($number) = $string =~ /(\d+\.\d+)/;
    return $number;
}

sub check() {
    my ( $this ) = @_;
    
    my $client = REST::Client->new();
    my $url = "$this->{baseUrl}/$this->{apiUrl}/$this->{wallet}\n";
    
    $client->setTimeout(20);
    $client->GET($url);
    my $response = $client->responseCode();
    if ($response == 200) {
    
        my $jsonResponse = from_json $client->responseContent();
        
        my $curHashRate = $this->getActualHashrate($jsonResponse->{'hashRate'});
        my $avgHashRate = $jsonResponse->{'avgHashrate'}/1000000;
        my $reportedHashRate = $this->getActualHashrate($jsonResponse->{'reportedHashRate'});
        INFO "Reported/Effective/Average hashrates: $reportedHashRate, $curHashRate, $avgHashRate\n";
        
        if ($reportedHashRate le $this->{alertThreshold}) {
            if ($this->{belowThresholdCount} ge $this->{maxBelowThreshold}) {
                DEBUG "System has been reset $this->{totalResets}/$this->{maxUnsuccessfulResets}";
            
                if ($this->{totalResets} ge $this->{maxUnsuccessfulResets}) {
                    ERROR "System needs reboot\n";
                    return "reboot";
                } else {
                    ERROR "Hashrate too low. Restarting mining system.\n";
                    return "down";
                }
            } else {
                $this->{belowThresholdCount} += 1;
                WARN "Hashrate dropped below threshold. Still within limit...\n";
                return "up";
            }
            
        } else {
            $this->{belowThresholdCount} = 0;
        }
        
    } else {
        WARN "Unable to read Etheremine pool stats (HTTP $response) - assuming the API is down.\n";
    }
    return "up";
}

sub resetComplete() {
    $this->{totalResets} += 1;
}

1;
