#!/usr/bin/perl 
package InternetMonitor;

use Log::Log4perl qw(:easy);
Log::Log4perl->init('log4perl.conf');

use Net::Ping;
use Time::HiRes;

sub new {

    my $class = shift;

    my $this = {
        url => shift,
        maxUnreachableCount => shift
    };
    
    if (!defined($this->{maxUnreachableCount})) {
        WARN "Max unreachable pings parameter not defined. Assuming default 20";
        $this->{maxUnreachableCount} = 5;
    }

    $this->{unreachableCallCount} = 0;
    $this->{pinger} = Net::Ping->new("external");
    
    bless $this, $class;
    INFO "Start monitoring host $this->{url}";
    if (!$this->check()) {
        WARN "Init check: host is unreachable\n";
    } else {
        INFO "Init check: host is reachable\n"
    }

    return $this;

}

sub resetVars() {
    my ( $this ) = @_;
    $this->{unreachableCallCount} = 0;
}

sub check {
    my ( $this ) = @_;
    
    my $url = $this->{url};
    #print "Pinging host $url.. ";
    
    my $p = $this->{pinger};
    $p->hires();
    ($ret, $duration, $ip) = $p->ping($url, 1);
    if ($ret) {
        DEBUG "Host $this->{url} reachable. Restult: $ret, $duration\n";
        $this->{unreachableCallCount} = 0;
        return "up";
    } else {
        if ($this->{unreachableCallCount}>$this->{maxUnreachableCount}) {
            ERROR "Host has been unreachable. Assuming network is down.\n";
        } else {
            $this->{unreachableCallCount} += 1;
            WARN "host is unreachable $duration. Still within limit $this->{unreachableCallCount}/$this->{maxUnreachableCount}\n";
            
            return "up";
        }
    }
    $p->close();
    return "down";
}

1;