#!/usr/bin/perl

package MinerWatchdog;

use Log::Log4perl qw(:easy);
Log::Log4perl->init('log4perl.conf');

use strict;
use Config::Simple;
use Getopt::Long;



# Private modules
use lib "modules";
use InternetMonitor;
use EtherminePoolChecker;
use EthProxyChecker;

# Default config
my $configFile="config.properties";

my $opt_config;
GetOptions ('c=s' => \$opt_config);
if (defined($opt_config)) {
    INFO "Config file is defined: $opt_config\n";
    $configFile=$opt_config;
} else {
    INFO "Reading default config $configFile\n";
}

my $cfg = new Config::Simple($configFile);


# Init internet monitor

my $wanMonIp = $cfg->param("wan_monitor_host");
if (!defined($wanMonIp)) {
    ERROR "Property wan.monitor.host is not defined. aborting\n";
    exit -1;
}

my $wanMonInterval = $cfg->param("wan_monitor_interval");
if (!defined($wanMonInterval)) {
    WARN "WAN monitor interval not defined. Using default (30s)\n";
    $wanMonInterval=30;
}

my $ethermineWallet = $cfg->param("ethermine_wallet_1");
if (!defined($ethermineWallet)) {
    ERROR "Property ethermine_wallet_1 is not defined. aborting\n";
    exit -1;
}

my $ethermineBaseUrl = $cfg->param("ethermine_base_url");
if (!defined($ethermineBaseUrl)) {
    ERROR "Property ethermine_base_url is not defined. aborting\n";
    exit -1;
}

my $ethermineApiUrl = $cfg->param("ethermine_api_url");
if (!defined($ethermineApiUrl)) {
    ERROR "Property ethermine_api_url is not defined. aborting\n";
    exit -1;
}

my $ethermineMinHashrate = $cfg->param("ethermine_miner_min_reported_hrate");
if (!defined($ethermineMinHashrate)) {
    ERROR "Property ethermine_miner_min_reported_hrate is not defined. aborting\n";
    exit -1;
}

my $ethermineMaxBelowTH = $cfg->param("ethermine_miner_max_below_threshold");
if (!defined($ethermineMaxBelowTH)) {
    ERROR "Property ethermine_miner_max_below_threshold is not defined. aborting\n";
    exit -1;
}

my $ethermineRefreshRate = $cfg->param("ethermine_miner_refresh_rate");
if (!defined($ethermineRefreshRate)) {
    ERROR "Property ethermine_miner_refresh_rate is not defined. aborting\n";
    exit -1;
}

my $stopMiningCmd = $cfg->param("stop_mining_command");
if (!defined($stopMiningCmd)) {
    ERROR "Property stop_mining_command is not defined. aborting\n";
    exit -1;
}

my $startMiningCmd = $cfg->param("start_mining_command");
if (!defined($startMiningCmd)) {
    ERROR "Property start_mining_command is not defined. aborting\n";
    exit -1;
}

# Instantiate monitors
my $pool1 = new EtherminePoolChecker($ethermineWallet, $ethermineBaseUrl, $ethermineApiUrl, $ethermineMinHashrate, $ethermineMaxBelowTH, 2);

my $monitor = new InternetMonitor($wanMonIp);

my $ethProxy = new EthProxyChecker("http://192.168.1.150:8080/version");

# At this point the constructors have performed the initial check. Assume everything's up
my $poolStatus = 'up';
my $wanStatus = 'up';


my $poolPid = fork(); # Unable to use perl threads - conflits with JSON lib
die if not defined $poolPid;
if (not $poolPid) {
        INFO "Spawned child process for Pool monitoring\n";
        minerMonitor($pool1);
        exit;
}

my $wanPid = fork();  # Unable to use perl threads - conflits with JSON lib
die if not defined $wanPid;
if (not $wanPid) {
        INFO "Spawned child for WAN monitoring\n";
        wanMonitor($monitor);
        exit;
}

#my $ethProxyPid = fork();  # Unable to use perl threads - conflits with JSON lib
#die if not defined $ethProxyPid;
#if (not $ethProxyPid) {
#        INFO "Spawned child for EthProxy monitoring\n";
#        ethProxyMonitor($ethProxy);
#        exit;
#}

my $finished = wait();
WARN "Watchdog process stopped\n";

sub minerMonitor {
    my ( $monitor ) = @_;
     while(1) {
        $poolStatus = $monitor->check();
        if ("down" eq $poolStatus) {
            ERROR "Problem reported in the pool. Restarting mining system\n";
            
            INFO "Run stop mining cmd: $stopMiningCmd\n";
            system($stopMiningCmd);
            sleep 10;
            
            INFO "Run start mining cmd: $startMiningCmd\n";
            system($startMiningCmd);
            
            $monitor->resetComplete();
        } elsif ("reboot" eq $poolStatus) {
            #system("reboot -f");
            ERROR "Rebooting..."
        }
        sleep $ethermineRefreshRate*60;
    }
}

sub wanMonitor {
    my ( $monitor ) = @_;
    
    while(1) {
        $monitor->check();
        sleep $wanMonInterval;
    }
}

#sub ethProxyMonitor {
#    my ( $monitor ) = @_;
#    
#    while(1) {
#        $proxyStatus = $monitor->check();
#        if (!$proxyStatus) {
#            INFO "Eth Proxy seems to be down\n";
#            
#            INFO "Run stop proxy cmd: $stopProxyCmd\n";
#            sleep 10;
#            
#            INFO "Run start proxy cmd: $startProxyCmd\n";
#            sleep 10;
#        }
        
#        sleep $wanMonInterval;
#    }
#}